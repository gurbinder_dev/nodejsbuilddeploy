var express = require('express');
var router = express.Router();
var mysql = require('../config/db');

/* GET home page. */
router.get('/', function(req, res, next) {
  // console.log(mysql);
  res.render('index', { title: 'Express Appp' });
});

var sql = 'select * from articles';

router.get('/get', function(req, res, next) {
  mysql.query(sql,function (err, result) {
    if(err)
    res.send({'error':true , 'message': err});
    else
    res.send({'success':true , 'data': result});
  });
});

module.exports = router;
